﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class phpUnityR : MonoBehaviour {

    public InfoManager infoManager;
    public InputField usernameInput;
    public InputField passwordInput;
    public InputField emailInput;
    public InputField confirmPass;
    public GameObject registerPanel;
    public Text messageText;

    void Start()
    {
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Return) && (registerPanel.activeSelf == true))
        {
            tryRegister();
        }
    }

    public void tryRegister()
    {
        messageText.text = string.Empty;
        string username;
        string password;
        string email;
        string confirm;
        username = usernameInput.text;
        password = passwordInput.text;
        email = emailInput.text;
        confirm = confirmPass.text;
        if (password.Length >= 8 && password.Length <= 16)
        {
            if (email.Contains("@") && (email.EndsWith(".com") || email.EndsWith(".org") || email.EndsWith(".net")))
            {
                if (String.Equals(confirm, password))
                {
                    StartCoroutine(Register(username, password, email));
                }
                else
                {
                    messageText.text = "\n**Passwords do not match**";
                }
            }
            else { messageText.text = "\n**Please use a valid email address**"; }
        }
        else { messageText.text = "**Please make password 8-16 characters in length**"; }
    }

    IEnumerator Register(string formUser, string formPassword, string formEmail)
    {
        string URL = "http://50.4.16.27:80/check_userR.php"; //change for your URL
        string hash = "xC6VsGdaF5RkqzT"; //change your secret code, and remember to change into the PHP file too
        string formText = "";//this field is where the messages sent by PHP script will be in
        WWWForm form = new WWWForm(); //here you create a new form connection
        form.AddField("myform_hash", hash); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
        form.AddField("myform_user", formUser);
        form.AddField("myform_pass", formPassword);
        form.AddField("myform_email", formEmail);

        WWW w = new WWW(URL, form); //here we create a var called 'w' and we sync with our URL and the form
        yield return w; //we wait for the form to check the PHP file, so our game dont just hang
        if (w.error != null)
        {
            print(w.error); //if there is an error, tell us
        }
        else
        {
            formText = w.text.Trim(); //here we return the data our PHP told us
            w.Dispose();
            print(formText);    
            string[] echo = formText.Split('\n');
            messageText.text = echo[0];
            infoManager.setUserID(int.Parse(echo[1]));
            usernameInput.text = string.Empty;
            passwordInput.text = string.Empty;
            emailInput.text = string.Empty;
            confirmPass.text = string.Empty;
        }
        formUser = string.Empty; //just clean our variables
        formPassword = string.Empty;
        formText = string.Empty;
    }
}
