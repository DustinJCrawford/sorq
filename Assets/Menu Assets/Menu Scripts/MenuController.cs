using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class MenuController : MonoBehaviour {

    public GameManager gameManager;
	public InfoManager infoManager;
    public GameObject MainCamera;
    public GameObject MenuBackground;
    public GameObject Managers;
    public float camDelay = 0.45f;
	EventSystem system;

    void Start()
    {
		system = EventSystem.current;
		infoManager = GameObject.Find ("InfoManager").GetComponent<InfoManager> ();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
			
			if (next != null)
			{
				
				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield != null)
					inputfield.OnPointerClick(new PointerEventData(system));  //if it's an input field, also set the text caret
				
				system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
			}
		}
	}
	public Font myFont;
	void OnGUI()
	{
		if (infoManager.getSignedIn ()) 
		{
			GUI.skin.font = myFont;
			GUI.skin.label.alignment = TextAnchor.UpperRight;
			GUI.Label (new Rect (Screen.width - 375, 0, 250, 200), "Display Name: " 
			           + infoManager.getDisplayName () + "\nRunes: " + infoManager.getMoney());
		}
	}

    public void DeckSelection()
    {
        iTween.MoveTo(MainCamera, iTween.Hash("x", 0, "y", 15, "time", camDelay));
    }

    public void Play()
    {
        iTween.MoveTo(MainCamera, iTween.Hash("x", 25, "y", 15, "time", camDelay));
    }

    public void Options()
    {
        iTween.MoveTo(MainCamera, iTween.Hash("x", 25, "y", 0, "time", camDelay));
    }

    public void Profile()
    {
        iTween.MoveTo(MainCamera, iTween.Hash("x", -25, "y", 0, "time", camDelay));
    }

    public void Home()
    {
        iTween.MoveTo(MainCamera, iTween.Hash("x", 0, "y", 0, "time", camDelay));
    }

    public void Lobby()
    {
        iTween.MoveTo(MainCamera, iTween.Hash("x", 42.5, "y", 15, "time", camDelay));
    }

	public void postGame()
	{
		MainCamera.transform.position = new Vector3 (42.5f, 15f, -10f);
	}

    public void LoadGameScene()
    {
        iTween.MoveTo(Managers, iTween.Hash("x", -45, "y", 0, "z", -15, "time", 0.0));
        gameManager.startGameFromLobby();
    }

	public void LoadDeckBuilder()
	{
		Application.LoadLevel (1);
        infoManager.playDeckBuilderMusic();
	}

    public void ExitGame()
    {
        Application.Quit();
    }
}
