﻿using UnityEngine;
using System.Collections;

public class ShowDatabase : MonoBehaviour {
	
	string cards = "loadAllCards.php";
	string users = "loadAllUsers.php";
	string assignments = "loadAssignments.php";
	
	void Start ()
	{
		//StartCoroutine(loadTable(users));
        //StartCoroutine(loadTable(cards));
		//StartCoroutine(loadTable(assignments));

	}
	
	public IEnumerator loadTable(string phpFile)
	{
		string url = "http://50.4.16.27:80/" + phpFile;
		WWW w = new WWW(url);
		yield return w;
		if (w.error != null)
		{
			print(w.error);
		}
		else
		{
			print(w.text);
		}
		w.Dispose();
	}
}
