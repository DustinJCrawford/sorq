﻿using UnityEngine;
using System.Collections;
using System.Security.AccessControl;


public class EffectManager : MonoBehaviour
{
    public GameManager gameManager;
    public AttackManager attackManager;
    public System.Random rnd;
	// Use this for initialization

    public bool addedKeeperEffect;
	void Start ()
	{
	    gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	    attackManager = GameObject.Find("AttackManager").GetComponent<AttackManager>();
	    rnd = new System.Random();
	}


    /*public void checkEffect(NetworkViewID viewID)
    {
        int cardID = NetworkView.Find(viewID).GetComponent<Card>().cardID;
        Debug.Log("Checking for Effect for card ID: " + cardID);
        switch (cardID)
        {
            case 20: // Bone Cluster
                if (attackManager.attackActive && attackManager.defendIDNum == cardID)
                {
                    gameManager.activeSpellCardID = 20;
                    attackManager.defenseBoost = attackManager.defendCardStr;
                }
                break;

            case 31: //Gift of the Saints
                gameManager.activeSpellCardID = 31;
                gameManager.addPlayerPower(1);
                break;

            case 139: //Archaeological Dig
                gameManager.activeSpellCardID = 139;
                gameManager.drawCard(2);
                break;

            case 81: //Insight
                gameManager.activeSpellCardID = 81;
                gameManager.drawCard(2);
                break;

			case 136: // Suit up!
                gameManager.activeSpellCardID = 136;
                // Temporary
                gameManager.destroyActiveSpell = true;
			    break;

            case 88: //Black Hole
                gameManager.activeSpellCardID = 88;
                gameManager.globalCardInPlayDestroy();
                gameManager.endTurn();
                break;

            case 46: //Leech Life
                gameManager.activeSpellCardID = 46;
                //gameManager.selectCard = true;
                //gameManager.addPlayerHP(2);
                break;

            case 33: //Righteous Axe
                gameManager.activeSpellCardID = 33;
                gameManager.destroyActiveSpell = true;
                break;

            case 41: //Blessed Armory
                gameManager.activeSpellCardID = 41;
                gameManager.increaseFriendlyMinionVit(2);
                break;

            case 28: //Keeper of Souls
                if (!addedKeeperEffect && gameManager.destroyingCard && !gameManager.destroySpell)
                {
                    gameManager.activeSpellCardID = cardID;
                    addStrKeeperOfSouls();
                }
                break;

            case 68: //Illusionist
                //10% chance to dodge enemy attacks
                if (attackManager.attackActive && attackManager.defendIDNum == cardID)
                {
                    if (rnd.Next(10) == 3)
                    {
                        attackManager.negateAttack = true;
                    }
                }
                break;

            case 15: //Hyperion, Lucent King
                //damage ALL minions for 1 VIT
                break;

            case 61: //Hundred Year Old Wizard
                //Spells cost 1 less PWR while in play
                break;

            case 75: //Archmage Volarus
                //When played, player may cast any spell in hand for free in the same turn.
                break;

            case 74: //Harbinger of Doom
                //Replenishes 1 PWR when a minion dies while in play.
                break;
			
			case 200: //Temporary implementation of Black Ice
                if (attackManager.attackActive)
                {
                    gameManager.activeSpellCardID = 200;
                    attackManager.negateAttack = true;
                    gameManager.destroyActiveSpell = true;
                }
                break;
        }
    }*/

    /*public void checkTrigger()
    {
        Card[] cards = GameObject.FindObjectsOfType<Card>();
        foreach (Card c in cards)
        {
            if (c.GetComponent<CardController>().cardInPlay)
            {
                checkEffect(c.networkView.viewID);
            }
        }
        GameObject[] spells = GameObject.FindGameObjectsWithTag("OpponentSpell");
        foreach(GameObject s in spells)
        {
            checkEffect(s.networkView.viewID);
        }

        addedKeeperEffect = false;
    }*/

    public void clearSpells()
    {
        GameObject[] itemsToRemove = GameObject.FindGameObjectsWithTag("PlayerSpell");
        for (int i = 0; i < itemsToRemove.Length; i++)
        {
            if (itemsToRemove[i].GetComponent<spellIconController>().srcCard.cardType == Card.cardTypes.spellTrig ||
                itemsToRemove[i].GetComponent<spellIconController>().srcCard.cardType == Card.cardTypes.spellCont) continue;
            Destroy(itemsToRemove[i]);
            gameManager.gameBoard.spellPosAvail[i] = true;
        }
    }
    public void addStrKeeperOfSouls()
    {
        Card[] cards = GameObject.FindObjectsOfType<Card>();
        for (int c = 0; c < cards.Length; c++)
        {
            if (cards[c].cardID == gameManager.activeSpellCardID && cards[c].GetComponent<CardController>().cardInPlay)
            {
                int newStr = cards[c].cardStr + 1;
                gameManager.networkView.RPC("updateCardStr", RPCMode.All, cards[c].networkView.viewID, newStr);
                addedKeeperEffect = true;
            }
        }
    }
}
