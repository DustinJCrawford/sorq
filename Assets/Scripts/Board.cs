﻿using UnityEngine;
using System.Collections;

public class Board {

    private Vector3 playZonePosition;
    private Vector3 enemyPlayZonePosition;
    private Vector3 handZonePosition;
    private Vector3 enemyHandZonePosition;
    private Vector3 snapPos;
    private Vector3 enemyViewSnapPos;
    private Vector3 spellPosition;
    private Vector3 enemySpellPos;

    private Vector3 returnVector;

    public Vector3[] cardPlayPositions;
    public Vector3[] enemyCardPlayPositions;
    public Vector3[] cardHandPositions;
    public Vector3[] enemyCardHandPositions;

    private Vector3[] spellPositions;
    private Vector3[] enemySpellPositions;

    public bool[] playPosAvail = new bool[5] { true, true, true, true, true };
    public bool[] enemyPlayPosAvail = new bool[5] { true, true, true, true, true };
    public bool[] handPosAvail = new bool[7] { true, true, true, true, true, true, true };
    public bool[] enemyHandPosAvail = new bool[7] { true, true, true, true, true, true, true };

    public bool[] spellPosAvail = new bool[3] { true, true, true };
    public bool[] enemySpellPosAvail = new bool[3] { true, true, true };

	public int handSpaceAvailable = 0;


	// Use this for initialization
	void Start () {
	
	}


    public void createPlayPositions()
    {
        playZonePosition = GameObject.Find("PlayZone").transform.position;
        cardPlayPositions = new Vector3[5];
        cardPlayPositions[0] = playZonePosition;
        cardPlayPositions[1] = new Vector3((playZonePosition.x - 65.0f), playZonePosition.y, playZonePosition.z);
        cardPlayPositions[2] = new Vector3((playZonePosition.x + 65.0f), playZonePosition.y, playZonePosition.z);
        cardPlayPositions[3] = new Vector3((playZonePosition.x - 130.0f), playZonePosition.y, playZonePosition.z);
        cardPlayPositions[4] = new Vector3((playZonePosition.x + 130.0f), playZonePosition.y, playZonePosition.z);

        enemyPlayZonePosition = GameObject.Find("enemyPlayZone").transform.position;
        enemyCardPlayPositions = new Vector3[5];
        enemyCardPlayPositions[0] = enemyPlayZonePosition;
        enemyCardPlayPositions[1] = new Vector3((enemyPlayZonePosition.x - 65.0f), enemyPlayZonePosition.y, enemyPlayZonePosition.z);
        enemyCardPlayPositions[2] = new Vector3((enemyPlayZonePosition.x + 65.0f), enemyPlayZonePosition.y, enemyPlayZonePosition.z);
        enemyCardPlayPositions[3] = new Vector3((enemyPlayZonePosition.x - 130.0f), enemyPlayZonePosition.y, enemyPlayZonePosition.z);
        enemyCardPlayPositions[4] = new Vector3((enemyPlayZonePosition.x + 130.0f), enemyPlayZonePosition.y, enemyPlayZonePosition.z);

        spellPositions = new Vector3[3];
        spellPosition = GameObject.Find("playerSpellZone").transform.position;
        spellPositions[0] = spellPosition;
        spellPositions[1] = new Vector3((spellPosition.x + 30.0f), spellPosition.y, spellPosition.z);
        spellPositions[2] = new Vector3((spellPosition.x - 30.0f), spellPosition.y, spellPosition.z);

        enemySpellPositions = new Vector3[3];
        enemySpellPos = GameObject.Find("enemySpellZone").transform.position;
        enemySpellPositions[0] = enemySpellPos;
        enemySpellPositions[1] = new Vector3((enemySpellPos.x + 30.0f), enemySpellPos.y, enemySpellPos.z);
        enemySpellPositions[2] = new Vector3((enemySpellPos.x - 30.0f), enemySpellPos.y, enemySpellPos.z);
    }

    public void createHandPositions()
    {
        handZonePosition = GameObject.Find("HandZone").transform.position;
        cardHandPositions = new Vector3[7];
        cardHandPositions[5] = new Vector3((handZonePosition.x + 195.0f), handZonePosition.y, handZonePosition.z);
        cardHandPositions[3] = new Vector3((handZonePosition.x + 130.0f), handZonePosition.y, handZonePosition.z);
        cardHandPositions[1] = new Vector3((handZonePosition.x + 65.0f), handZonePosition.y, handZonePosition.z); ;
        cardHandPositions[0] = handZonePosition;
        cardHandPositions[2] = new Vector3((handZonePosition.x - 65.0f), handZonePosition.y, handZonePosition.z);
        cardHandPositions[4] = new Vector3((handZonePosition.x - 130.0f), handZonePosition.y, handZonePosition.z);
        cardHandPositions[6] = new Vector3((handZonePosition.x - 195.0f), handZonePosition.y, handZonePosition.z);

        enemyHandZonePosition = GameObject.Find("enemyHandZone").transform.position;
        enemyCardHandPositions = new Vector3[7];
        enemyCardHandPositions[5] = new Vector3((enemyHandZonePosition.x + 195.0f), enemyHandZonePosition.y, enemyHandZonePosition.z);
        enemyCardHandPositions[3] = new Vector3((enemyHandZonePosition.x + 130.0f), enemyHandZonePosition.y, enemyHandZonePosition.z);
        enemyCardHandPositions[1] = new Vector3((enemyHandZonePosition.x + 65.0f), enemyHandZonePosition.y, enemyHandZonePosition.z);
        enemyCardHandPositions[0] = enemyHandZonePosition;
        enemyCardHandPositions[2] = new Vector3((enemyHandZonePosition.x - 65.0f), enemyHandZonePosition.y, enemyHandZonePosition.z);
        enemyCardHandPositions[4] = new Vector3((enemyHandZonePosition.x - 130.0f), enemyHandZonePosition.y, enemyHandZonePosition.z);
        enemyCardHandPositions[6] = new Vector3((enemyHandZonePosition.x - 195.0f), enemyHandZonePosition.y, enemyHandZonePosition.z);

		handSpaceAvailable = 7;

    }

    public Vector3 calcPlayPosition()
    {

        for (int i = 0; i < cardPlayPositions.Length; i++)
        {
            if (playPosAvail[i] == true)
            {
                returnVector = cardPlayPositions[i];
                playPosAvail[i] = false;
                break;
            }

        }

        return returnVector;
    }



    public Vector3 calcSpellPlayPosition()
    {
        for (int i = 0; i < spellPositions.Length; i++)
        {
            if (spellPosAvail[i] == true)
            {
                returnVector = spellPositions[i];
                spellPosAvail[i] = false;
                break;
            }
        }

        return returnVector;
    }


    public Vector3 calcSpellPlayPositionForEnemy()
    {
        for (int i = 0; i < enemySpellPositions.Length; i++)
        {
            if (enemySpellPosAvail[i] == true)
            {
                returnVector = enemySpellPositions[i];
                enemySpellPosAvail[i] = false;
                break;
            }
        }

        return returnVector;
    }


    public Vector3 calcPlayPositionForEnemy()
    {
        for (int i = 0; i < enemyCardPlayPositions.Length; i++)
        {
            if (enemyPlayPosAvail[i] == true)
            {
                returnVector = enemyCardPlayPositions[i];
                enemyPlayPosAvail[i] = false;
                break;
            }
        }

        return returnVector;
    }

    // Used for Minions
    public bool isPlaySpaceAvailable()
    {
        for (int i = 0; i < playPosAvail.Length; i++)
        {
            if (playPosAvail[i] == true)
            {
                return true;
            }
        }

        return false;
    }

    public bool isPlaySpotAvailable(int index)
    {
        if (playPosAvail[index] == true)
        {
            return true;
        }

        else return false;
    }


    // Used for Spells
    public bool isSpellSpaceAvailable()
    {
        for (int i = 0; i < spellPosAvail.Length; i++)
        {
            if (spellPosAvail[i] == true)
            {
                return true;
            }
        }

        return false;
    }


    public Vector3 calcHandPosition()
    {

        for (int i = 0; i < cardHandPositions.Length; i++)
        {
            if (handPosAvail[i] == true)
            {
                returnVector = (Vector3)cardHandPositions[i];
                handPosAvail[i] = false;
                break;
            }
        }

        return returnVector;

    }

    public Vector3 calcHandPositionForEnemy()
    {
        for (int i = 0; i < enemyCardHandPositions.Length; i++)
        {
            if (enemyHandPosAvail[i] == true)
            {
                returnVector = (Vector3)enemyCardHandPositions[i];
                enemyHandPosAvail[i] = false;
                break;
            }
        }

        return returnVector;
    }

    public bool isHandSpaceAvailable()
    {
        for (int i = 0; i < handPosAvail.Length; i++)
        {
            if (handPosAvail[i] == true)
            {
                return true;
            }
        }

        return false;
    }

    public Vector3 findNearestPlayPosition(Vector3[] vectArray, Vector3 currentPos)
    {
        float[] distArray = new float[vectArray.Length];
        int smallestIndex;

        for (int i = 0; i < vectArray.Length; i++)
        {
            distArray[i] = vectArray[i].x - currentPos.x;
        }

        for (int i = 0; i < distArray.Length; i++)
        {

            if (i > 0 && (distArray[i] < distArray[i - 1]))
            {
                smallestIndex = i;
                returnVector = vectArray[smallestIndex];
            }

        }

        return returnVector;
    }





}
