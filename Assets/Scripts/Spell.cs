﻿using UnityEngine;
using System.Collections;


public class Spell : MonoBehaviour {

	public string spellName { get; set; }
	public int spellEffectID { get; set; }
	public int spellDuration;
	public int spellDamage;
	public int spellHealing;

	public enum SpellTypes 
	{
		spellNorm,
		spellEqp,
		spellTrig,
		spellCont
	}

	public SpellTypes spellType;

    public Spell()
    {

    }

    public Spell(string name, SpellTypes type)
    {
        this.spellName = name;
        this.spellType = type;
    }

	void Start() 
	{

	}

	void Update() 
	{

	}

	public void dealDamage(GameObject target, int amount) 
	{

	}

    public void drawCard(int amount)
    {
        
    }

	public void healDamage() 
	{

	}

	public void dazeMinion() 
	{

	}

	public void givePierce() 
	{

	}
}
