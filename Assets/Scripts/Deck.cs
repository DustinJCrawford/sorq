﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Deck {

    public List<Card> deck = new List<Card>();
	public List<Card> discardDeck = new List<Card>();
    private string[] cards = new string[30];
    public bool initialized;
    public int SHUFFLE = 5; // Shuffle iteration constant
    public System.Random r = new System.Random();
    private int size = 0;


    public IEnumerator getDeck(int deckID)
    {
        Debug.Log("Deck.getDeck() called");
        if (!initialized)
        {

            //CARD_ID | CARD_NAME | CARD_PWR | CARD_STR | CARD_VIT | CARD_EQP | CARD_ATRB | CARD_TYPE | CARD_DESC
            //temp[0] | temp[1]   | temp[2]  | temp[3]  | temp[4]  | temp[5]  | temp[6]   | temp[7]   | temp[8]

            string formText;
            string url = "http://50.4.16.27:80/loadDeck.php";
            WWWForm form = new WWWForm();
            form.AddField("myform_deck", deckID);
            WWW w = new WWW(url, form);
            yield return w; //we wait for the form to check the PHP file, so our game dont just hang
            Debug.Log("Form text\n" + w.text);
            formText = w.text; //here we return the data our PHP told us
            cards = formText.Split('\n');
            w.Dispose(); //clear our form in game
            formText = "";

            for (int i = 1; i < cards.Length - 1; i++)
            {
                string[] temp = cards[i].Split(';');
                bool spell;
                switch (temp[7])
                {
                    case "SPELL-N": case "SPELL-T": case "SPELL-C": case "SPELL-E":
                        spell = true;
                        break;
                    default:
                        spell = false;
                        break;
                }

                Card newCard = new Card(int.Parse(temp[0]), temp[1], int.Parse(temp[2]), int.Parse(temp[3]), int.Parse(temp[4]),
                    int.Parse(temp[5]), temp[6], temp[8], spell, temp[7]);
                deck.Add(newCard);

            }

            initialized = true;

        }
    }
	
	public void addSpecialCard()
    {

	}

    public int Size()
    {
        return this.size;
    }
    public void increaseSize()
    {
        this.size++;
    }
    public void addCard(Card input)
    {
        deck.Add(input);
        increaseSize();
    }

    public void addDiscard(Card input)
    {
        Debug.Log("Added to discard: " + input.cardName);
        discardDeck.Add(input);
    }

    public Card getCard()
    {
        if (deck.Count == 0)
        {
            this.deck = this.discardDeck;
            this.shuffle();
            this.discardDeck.Clear();
        }
        Card temp = deck[0];
        deck.RemoveAt(0);
        return temp;
    }



    public Card getCardFromDiscard()
    {

        Card temp;
        System.Random rand = new System.Random();
		int randNum = rand.Next (0, this.discardDeck.Count);
        temp = discardDeck[randNum];
		discardDeck.RemoveAt (randNum);

        return temp;
    }


    public void shuffle()
    {
        for (int i = 0; i < SHUFFLE; i++)
        {
            this.deck = shuffleR(this.deck);
        }
    }

    private List<Card> shuffleR(List<Card> inputDeck)
    {
        List<Card> newOrder = new List<Card>();
        while (inputDeck.Count >= 1)
        {
            int selection = r.Next(inputDeck.Count);
            newOrder.Add(inputDeck[selection]);
            inputDeck.RemoveAt(selection);
        }
        return newOrder;
    }
}
