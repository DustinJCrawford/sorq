using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InfoManager : MonoBehaviour {
    
    //SINGLETON PATTERN TO MAKE SURE ONLY ONE INFOMANAGER EXISTS
    //AND THAT IT STAYS THROUGHOUT ENTIRETY OF GAMEPLAY
    public static InfoManager infoManager;
    void Awake()
    {
        if (!infoManager)
        {
            infoManager = this;
            DontDestroyOnLoad(infoManager);
        }
        else
        {
            Destroy(this.gameObject);
        }

        playMenuMusic();
    }

    #region AudioHandling
    public AudioSource musicSource;
    public AudioClip menuMusic;
    public AudioClip deckBuilderMusic;
    public AudioClip gameMusic;
    public void playMenuMusic()
    {
        musicSource.clip = menuMusic;
        musicSource.Play();
    }
    public void playDeckBuilderMusic()
    {
        musicSource.clip = deckBuilderMusic;
        musicSource.Play();
    }
    public void playGameMusic()
    {
        musicSource.clip = gameMusic;
        musicSource.Play();
    }
    #endregion

    #region privateVariables
    //VARIABLE INFORMATION WHICH IS TO BE KEPT UNTIL IMMORTIA IS CLOSED
    private string Username { get; set; }
    private string displayName { get; set; }
    private string playerFName { get; set; }
    private string playerLName { get; set; }
    private string playerEmail { get; set; }
	private string[] deckInfo { get; set; }
    private int UserID { get; set; }
    private int DeckID { get; set; }
    private string deckName { get; set; }
    private int playerWins { get; set; }
    private int playerLosses { get; set; }
    private int playerMatches { get; set; }
    private int playerMoney { get; set; }
	private bool signedIn{ get; set; }
	private bool postGame{ get; set; }
#endregion

    #region gameStatVariables
    private int playerMinions { get; set; }
    private int playerSpells { get; set; }
    private int playerKills { get; set; }
    private int opponentMinions { get; set; }
    private int opponentSpells { get; set; }
    private int opponentKills { get; set; }
    #endregion

    #region getGameStats
    public int getPlayerMinions()
    {
        return this.playerMinions;
    }
    public int getPlayerSpells()
    {
        return this.playerSpells;
    }
    public int getPlayerKills()
    {
        return this.playerKills;
    }
    public int getOpponentMinions()
    {
        return this.opponentMinions;
    }
    public int getOpponentSpells()
    {
        return this.opponentSpells;
    }
    public int getOpponentKills()
    {
        return this.opponentKills;
    }
    #endregion

    #region setGameStats
    public void initializeGameStats()
    {
        this.playerMinions = 0;
        this.playerSpells = 0;
        this.playerKills = 0;
        this.opponentMinions = 0;
        this.opponentSpells = 0;
        this.opponentKills = 0;
    }

    public void setPlayerMinions(int minions)
    {
        this.playerMinions = minions;
    }
    public void setPlayerSpells(int spells)
    {
        this.playerSpells = spells;
    }
    public void setPlayerKills(int kills)
    {
        this.playerKills = kills;
    }
    public void setOpponentMinions(int minions)
    {
        this.opponentMinions = minions;
    }
    public void setOpponentSpells(int spells)
    {
        this.opponentSpells = spells;
    }
    public void setOpponentKills(int kills)
    {
        this.opponentKills = kills;
    }
    #endregion

    #region getValues
    //RETURN REQUESTED INFORMATION
	public string getDisplayName()
	{
		return this.displayName;
	}
	public string getFName()
	{
		return this.playerFName;
	}
	public string getLName()
	{
		return this.playerLName;
	}
	public string getEmail()
	{
		return this.playerEmail;
	}
    public int getUserID()
    {
        return this.UserID;
    }
    public int getDeckID()
    {
        return this.DeckID;
    }
    public string getDeckName()
    {
        return this.deckName;
    }
    public int getWins()
    {
        return this.playerWins;
    }
    public int getLosses()
    {
        return this.playerLosses;
    }
    public int getMatches()
    {
        return (this.playerWins + this.playerLosses);
    }
    public int getMoney()
    {
        return this.playerMoney;
    }
    public bool getSignedIn()
    {
        return this.signedIn;
    }
	public bool getPostGame()
	{
		return this.postGame;
	}
	public string[] getDeckInfo()
	{
		return this.deckInfo;
	}
    #endregion

    #region setValues
    //SET INFORMATION TO BE STORED THROUGHOUT GAMEPLAY
    public void setUserID(int userid)
    {
        this.UserID = userid;
    }
    public void setUserName(string user)
    {
        this.Username = user;
    }
    public void setDisplayName(string display)
    {
        this.displayName = display;
    }
    public void setPlayerFName(string first)
    {
        this.playerFName = first;
    }
    public void setPlayerLName(string last)
    {
        this.playerLName = last;
    }
    public void setPlayerEmail(string email)
    {
        this.playerEmail = email;
    }    
    public void setDeckID(int DID)
    {
        this.DeckID = DID;
    }
    public void setDeckName(string deckname)
    {
        this.deckName = deckname;
    }
    public void setPlayerWins(int wins)
    {
        this.playerWins = wins;
    }
    public void setPlayerLosses(int losses)
    {
        this.playerLosses = losses;
    }
    public void setPlayerMoney(int money)
    {
        this.playerMoney = money;
    }
    public void setSignedIn(bool tru)
    {
        this.signedIn = tru;
    }
	public void setPostGame(bool tru)
	{
		this.postGame = tru;
	}
    public void setDeckInfo(string[] deck)
    {
        this.deckInfo = deck;
    }
    #endregion

    public IEnumerator updateSignOut(int userid)
    {
        string url = "http://50.4.16.27:80/offline.php";
        WWWForm form = new WWWForm();
        form.AddField("myform_hash", "xC6VsGdaF5RkqzT");
        form.AddField("myform_userid", userid);
        WWW w = new WWW(url, form);
        yield return w;
        if (w.error != null)
        {
            print(w.error);
        }
        else
        {
            print(w.text);
        }
    }
}
