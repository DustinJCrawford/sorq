﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class spellIconController : MonoBehaviour {

    public GameManager gameManager;
    public Text spellInfoText;
    public Text spellTypeText;
    public Text spellNameText;
    Vector3 mousePos;
    float x;
    float y;
    public string spellText;
    public string spellType;
    public string spellName;
    public int spellLength;
	public int cardID;
    public bool inPlay;
    public Card srcCard;
    public enum Type { icon, token};

    public Type iconType;

    // Use this for initialization
	void Start () 
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        x = mousePos.x;
        y = mousePos.y;
	}


    void OnMouseOver()
    {
        
    }

    void OnMouseEnter()
    {
        Debug.Log("Moused enter.");
        gameManager.spellInfoPanel.SetActive(true);
        spellInfoText = GameObject.Find("spellInfoText").GetComponent<Text>();
        spellTypeText = GameObject.Find("spellTypeText").GetComponent<Text>();
        spellNameText = GameObject.Find("spellNameText").GetComponent<Text>();

        spellNameText.text = spellName;
        spellTypeText.text = convertTypeText(spellType);
        spellInfoText.text = spellText;
    }

    void OnMouseExit()
    {
        gameManager.spellInfoPanel.SetActive(false);
        Debug.Log("Mouse exit.");
        spellTypeText.text = string.Empty;
        spellInfoText.text = string.Empty;
        spellNameText.text = string.Empty;
    }

    string convertTypeText(string textToConvert)
    {
        string returnString;

        if (textToConvert.Equals("spellNorm"))
        {
            returnString = "Normal";
            return returnString;
        }

        else if (textToConvert.Equals("spellEqp"))
        {
            returnString = "Equip";
            return returnString;
        }

        else if (textToConvert.Equals("spellTrig"))
        {
            returnString = "Trigger";
            return returnString;
        }

        else if (textToConvert.Equals("spellCont"))
        {
            returnString = "Continuous";
            return returnString;
        }

        else
        {
            returnString = "????";
            return returnString;
        }
    }

}
