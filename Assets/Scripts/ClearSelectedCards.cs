﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ClearSelectedCards : MonoBehaviour {

    public Transform availableCardsPanel;
    public Transform cardsInDeckPanel;
    public DeckBuilder deckBuilder;
    public Color unselectedColor;


	// Use this for initialization
	void Start () {
        unselectedColor = Color.white;
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void clearSelected()
    {
        int availableCardsCount = availableCardsPanel.transform.childCount;
        int deckCardsCount = cardsInDeckPanel.transform.childCount;

        for (int i = 0; i < availableCardsCount; i++)
        {
            availableCardsPanel.transform.GetChild(i).GetComponent<Image>().color = unselectedColor;

            deckBuilder.cardsToList.RemoveRange (0, deckBuilder.cardsToList.Count);
            availableCardsPanel.transform.GetChild(i).GetComponent<CardButtonController>().buttonSelected = false;
        }

        for (int i = 0; i < deckCardsCount; i++)
        {
            cardsInDeckPanel.transform.GetChild(i).GetComponent<Image>().color = unselectedColor;

			deckBuilder.cardsToDeck.RemoveRange (0, deckBuilder.cardsToDeck.Count);
            cardsInDeckPanel.transform.GetChild(i).GetComponent<CardButtonController>().buttonSelected = false;

        }

    }

}
