﻿using UnityEngine;
using System.Collections;

public class StateMachine {

    public enum State
    {
        Idle,
        Player_Turn,
        Player_Attacking,
        Player_Summoning,
        Player_Casting,
        Enemy_Turn,
        Enemy_Attacking,
        Enemy_Summoning,
        Enemy_Casting
    }

    State currentState = State.Idle;




	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
