﻿using UnityEngine;
using System.Collections;

public class HolyArrow : Spell, ISpell {

    GameManager gameManager;
    InfoManager infoManager;
	HolyArrow holyArrowSpell;
	private bool spellIsCast = false;
    private const int DAMAGE_AMOUNT = 2;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
		holyArrowSpell = new HolyArrow ();
        holyArrowSpell.spellName = "Holy Arrow";
        holyArrowSpell.spellType = Spell.SpellTypes.spellNorm;
        holyArrowSpell.spellDamage = DAMAGE_AMOUNT;
    }

	void Update() 
	{
		if (gameObject.GetComponent<CardController> ().cardInPlay && !spellIsCast) 
		{
			gameObject.GetComponent<CardController>().findPosInHand();
			gameManager.gameBoard.handPosAvail[gameObject.GetComponent<CardController>().posInHand] = true;
			castSpell();
		}
	}

    public void castSpell()
    {
        gameManager.enemyPlayer.playerHealth -= holyArrowSpell.spellDamage;
        gameManager.networkView.RPC("updateEnemyHealth", RPCMode.Others, gameManager.enemyPlayer.playerHealth);

        GameObject spellIcon;
        Vector3 spellTokenPos;
        spellTokenPos = gameManager.gameBoard.calcSpellPlayPosition();
        spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
        spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
        spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
        spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
        spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
        spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
        spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
        spellIcon.gameObject.tag = "PlayerSpell";

        infoManager.setPlayerSpells(infoManager.getPlayerSpells() + 1);

        gameObject.GetComponent<CardController>().networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);
        //gameManager.networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);

		gameManager.networkView.RPC("destroyCard", RPCMode.All, gameObject.networkView.viewID);
		spellIsCast = true;
    }
}
