﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameMenuController : MonoBehaviour {

    Player player;
    public Button endTurnButton;
    public Button enemyOptionsButton;
    GameManager gameManager;

	// Use this for initialization
	void Start () {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        endTurnButton = GameObject.Find("endTurnButton").GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
        if (gameManager.isPlayerTurn)
        {
            endTurnButton.interactable = true;
        }

        if (!gameManager.isPlayerTurn)
        {
            endTurnButton.interactable = false;
        }

	}



    public void drawCardFromDeck()
    {
       
        //gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        //gameManager.drawCard(1);
    }


    public void quitGame()
    {
		gameManager.goToPostGameLobby ();
		gameManager.networkView.RPC ("destroyGameManager", RPCMode.All);
    }

    public void disableButton()
    {
        gameObject.GetComponent<Button>().interactable = false;
    }

    public void endTurn()
    {
        //gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.endTurn();
        
    }

}
