﻿using UnityEngine;
using System.Collections;

public class Player {

    private const int MAX_PWR = 8;
    private const int MAX_HP = 25;

    public string playerDisplayName;
    public string playerUserName;
    public int playerUserID;
    public int playerHealth = MAX_HP;

    public int playerCurrentPower;
    public int playerNextTurnPower;
    public int playerMaxPower = MAX_PWR;

    public bool isTurn;
    public bool isFirstTurn;

    public Deck playerDeck;


    



}
