﻿using UnityEngine;
using System.Collections;

public class PanelFadeScript : MonoBehaviour {

    //public GameManager gameManager;
    public GameObject gameResultsPanel;
    float fadeOutTime = 10.0f; 
    float alpha = 0;
    bool includeChildren = false;

	// Use this for initialization
	void Start () {
        gameResultsPanel.SetActive(true);
        StartCoroutine(FadeOut(fadeOutTime, renderer.material));
    }

    IEnumerator FadeOut(float time, Material material)
    {
        float index = 0.0f;
        float rate = 1.0f / time;
        while (index < 1.0f)
        {
            index += Time.deltaTime * time;
            material.SetFloat("_Cutoff", index);
            yield return null;
        }
    }
}
