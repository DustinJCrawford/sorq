﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileUpdate : MonoBehaviour
{
    //HASH NEEDED TO COMMUNICATE WITH PHP FILES ON SERVER END
    private string hash = "xC6VsGdaF5RkqzT";//secret code to communicate with php file at server
    public InfoManager infoManager;
    public InputField displayInput;
    public InputField firstInput;
    public InputField lastInput;
    public Text displayNameLabel;
    public Text messageText;
    public Text displayName;
    public Text fname;
    public Text lname;
    public Text email;
    public GameObject applyButton;
    public GameObject editButton;
    public GameObject textObjects;
    public GameObject editObjects;

    void Start()
    {
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
    }
    //METHOD CALLED WHEN SIGN-IN BUTTON IS PRESSED
    //STARTS LOGIN COROUTINE
    public void updateProfileInfo()
    {
        int userID = infoManager.getUserID();
        string display;
        string first;
        string last;
        string email;

        display = displayInput.text.Trim();
        first = firstInput.text.Trim();
        last = lastInput.text.Trim();
        email = infoManager.getEmail();

        StartCoroutine(update(userID, display, first, last, email));
    }

    //COROUTINE FOR SENDING USERNAME AND PASSWORD TO DATABASE FOR COMPARISON
    IEnumerator update(int userID, string formDisplay, string formFirst, string formLast, string formEmail)
    {
        string formText = string.Empty;
        string url = "http://50.4.16.27:80/updateProfileInfo.php"; //change for your URL
        WWWForm form = new WWWForm(); //here you create a new form connection
        form.AddField("myform_hash", hash); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
        form.AddField("myform_userid", userID);
        form.AddField("myform_display", formDisplay);
        form.AddField("myform_fname", formFirst);
        form.AddField("myform_lname", formLast);
        form.AddField("myform_email", formEmail);
        WWW w = new WWW(url, form); //here we create a var called 'w' and we sync with our URL and the form
        yield return w; //we wait for the form to check the PHP file, so our game dont just hang
        if (w.error != null)
        {
            print(w.error); //if there is an error, tell us
        }
        else
        {
            formText = w.text.Trim(); //here we return the data our PHP told us
            if (!formText.Contains("**"))
            {
                infoManager.setDisplayName(formDisplay);
                displayNameLabel.text = "Username: " + infoManager.getDisplayName();
                displayName.text = formDisplay;
                fname.text = formFirst;
                lname.text = formLast;
                email.text = formEmail;
                setApplied();
            }
            StartCoroutine(ShowMessage(formText, 5.0f));
        }
        userID = 0; //just clean our variables
        formDisplay = string.Empty;
        formFirst = string.Empty;
        formLast = string.Empty;
        formEmail = string.Empty;
        w.Dispose();
    }

    public void setEdit()
    {
        textObjects.SetActive(false);
        editButton.SetActive(false);
        editObjects.SetActive(true);
        applyButton.SetActive(true);
    }

    void setApplied()
    {
        textObjects.SetActive(true);
        editButton.SetActive(true);
        editObjects.SetActive(false);
        applyButton.SetActive(false);
    }

    IEnumerator ShowMessage(string message, float delay)
    {
        messageText.text = message;
        yield return new WaitForSeconds(delay);
        messageText.text = string.Empty;
    }
}
